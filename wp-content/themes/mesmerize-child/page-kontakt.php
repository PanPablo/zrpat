<?php mesmerize_get_header(); ?>

<div id="map"></div>
<script>
    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        var uluru = {lat: 52.219794, lng: 21.016836};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 17, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }

</script>
<div class="contact-box">
    <div class="contact-data">
        <?php the_field('contact-data') ?>
    </div>
    <div class="form">
        <h4>SKONTAKTUJ SIĘ Z NAMI</h4>
        <?php the_field('form') ?>
    </div>
</div>
<?php get_footer(); ?>
