<?php
/**
 * Created by PhpStorm.
 * User: pawelstruminski
 * Date: 29/11/2018
 * Time: 23:37
 */

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

?>